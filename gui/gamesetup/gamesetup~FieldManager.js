init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    const showAgain = Engine.ConfigDB_GetValue("user", "fieldmanager.showterms");
    if (showAgain === undefined || showAgain !== "false")
        Engine.PushGuiPage("page_fieldmanager_terms.xml")
}});
