# About FieldManager

*FieldManager* is a [0 A.D.](https://play0ad.com) mod adding extra functionalities for efficient management of field gatherers.

**WARNING:** this mod executes automated actions. This can be considered as a cheat. When you play a game with this mod, make sure other players are aware you are using this mod and agree.

# Features

* Select military/non-military/all gatherers from selected fields.
* Select one gatherer from each selected field.
* Evenly distribute gatherers across selected fields.

# Installation

[Click here](https://gitlab.com/mentula0ad/FieldManager/-/releases/permalink/latest/downloads/fieldmanager.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/FieldManager/-/releases/permalink/latest/downloads/fieldmanager.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/FieldManager/-/releases/permalink/latest/downloads/fieldmanager.zip) | [Older Releases](https://gitlab.com/mentula0ad/FieldManager/-/releases)

# Questions & feedback

For more information, questions and feedback, visit the [thread on the 0 A.D. forum](https://wildfiregames.com/forum/topic/106399-fieldmanager-mod-reduce-field-harvesting-micro-management/).
